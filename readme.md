# CSGO CFG

### CFG内容

请使用记事本或其他编辑器打开/mycfg/mods中的cfg文件查看，可以通过文件名判断各个cfg文件负责的功能，通过文件中的注释来了解实现的功能以及各个键位对应的功能。

### 使用方法

将mycfg文件夹放在CSGO游戏目录/csgo/cfg下，进入游戏，打开控制台执行以下命令。

```
exec mycfg/index
```